# kelas-pintar

Test Backend Developer

## Getting started

1. Move yourself to the backend folder: `cd mini-project`
2. Copy the .env_example file and create a .env file and add the connection DB and other and SIGNATURE (can be any word)
3. Install sequelize-cli how dev-dependency and execute this `sequelize db:create` next `sequelize db:migrate` and `sequelize db:seed:all`
4. Install node-modules `$ npm i` and run with command `$ npm start`

# List API

Services API, deployed in heroku:
| No | Routes | EndPoint | Description |
| -- | ------ | ------------------------------------------------------------- | -------------------------------------------------------- |
| 1. | POST | https://app-kelas-pintar.herokuapp.com/api/generateToken | Generate Token for authentication |
| 2. | GET | https://app-kelas-pintar.herokuapp.com/api/reportAllStudent | Objective: 1.A |
| 3. | GET | https://app-kelas-pintar.herokuapp.com/api/topThreeStudent | Objective: 1.B |
| 4. | GET | https://app-kelas-pintar.herokuapp.com/api/listStudentsScoreBelowAverage | Objective: 1.B |
| 5. | GET | https://app-kelas-pintar.herokuapp.com/api/reportStudentPerId/:id | for Teacher |
| 6. | GET | https://app-kelas-pintar.herokuapp.com/api/myReport | Report for student |

Link doc postman : https://documenter.getpostman.com/view/17902429/UVJZoJC4
