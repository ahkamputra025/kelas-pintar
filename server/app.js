require("dotenv").config();
const express = require("express");
const cors = require("cors");

const app = express();
app.use(cors());
app.use(express.urlencoded({ extended: false }));
app.use(express.json());

const teacherRoutes = require("./src/routes/teacher.routes"),
  studentRoutes = require("./src/routes/student.route"),
  generetaTokenRoute = require("./src/routes/generateToken");
app.use("/api/", teacherRoutes, studentRoutes, generetaTokenRoute);

app.get("/", (req, res) => res.send("Welcome to app API Kelas Pintar!"));
app.get("*", (req, res) =>
  res.send("You've tried reaching a route that doesn't exist.")
);

const port = process.env.PORT || 3000;
app.listen(port, () => {
  console.log(`Server Running, mode on port ${port}`);
});
