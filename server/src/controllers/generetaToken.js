const { teachers, students } = require("../database/models");
const jwt = require("jsonwebtoken");
const method = {};

method.generateToken = async (req, res) => {
  const { id } = req.body;
  try {
    const cekDataTeacher = await teachers.findAll({
      where: { teacher_id: id },
    });
    if (cekDataTeacher[0] === undefined) {
      const cekDataStudent = await students.findAll({
        where: { student_id: parseInt(id) },
      });
      if (cekDataStudent[0] === undefined) {
        res.status(401).json({
          statusCode: 401,
          statusText: "failed",
          message: "Id Teacher or Student not registered.",
        });
      } else {
        const valDataJwt = {
          student_id: cekDataStudent[0].student_id,
          name: cekDataStudent[0].name,
          role: "student",
        };
        const token = jwt.sign(
          {
            valDataJwt,
          },
          process.env.SECRET_KEY,
          {
            expiresIn: "30d",
          }
        );
        res.status(200).json({
          statusCode: 200,
          statusText: "success",
          message: "Generate Token student success.",
          result: cekDataStudent,
          token: token,
        });
      }
    } else {
      const valDataJwt = {
        student_id: cekDataTeacher[0].teacher_id,
        name: cekDataTeacher[0].name,
        role: "teacher",
      };
      const token = jwt.sign(
        {
          valDataJwt,
        },
        process.env.SECRET_KEY,
        {
          expiresIn: "30d",
        }
      );
      res.status(200).json({
        statusCode: 200,
        statusText: "success",
        message: "Generate Token Teacher success.",
        result: cekDataTeacher,
        token: token,
      });
    }

    // console.log(typeof id);
    // res.status(200).json({
    //   statusCode: 200,
    //   statusText: "success",
    //   message: "Generate Token Teacher success.",
    //   result: id,
    // });
  } catch (error) {
    res.status(400).send(error.message);
  }
};

module.exports = method;
