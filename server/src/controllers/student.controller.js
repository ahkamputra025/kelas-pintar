const { reports, students, subjects, chapters } = require("../database/models");
const method = {};

method.myReport = async (req, res) => {
  try {
    const resData = await reports.findAll({
      attributes: ["grade", "score"],
      where: { student_id: req.token.valDataJwt.student_id },
      include: [
        { model: students, attributes: ["name"] },
        { model: subjects, attributes: ["subject_name"] },
        { model: chapters, attributes: ["chapter_name"] },
      ],
      order: [["report_id", "ASC"]],
    });
    res.status(200).json({
      statusCode: 200,
      statusText: "success",
      message: "Get My Report success.",
      result: resData,
    });
  } catch (error) {
    res.status(400).send(error.message);
  }
};

module.exports = method;
