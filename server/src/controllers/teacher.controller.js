const {
  reports,
  subjects,
  chapters,
  students,
  Sequelize,
} = require("../database/models");
const { Op } = require("sequelize");
const method = {};

method.reportAllStudent = async (req, res) => {
  try {
    const resData = await reports.findAll({
      attributes: [
        "grade",
        [Sequelize.fn("count", Sequelize.col("student_id")), "total_student"],
        [Sequelize.fn("SUM", Sequelize.col("score")), "total_score"],
        [
          Sequelize.fn("ROUND", Sequelize.fn("avg", Sequelize.col("score")), 1),
          "avg_score",
        ],
      ],
      group: [
        "reports.subject_id",
        "reports.grade",
        "reports.chapter_id",
        "subject.subject_id",
        "chapter.chapter_id",
      ],
      include: [
        { model: subjects, attributes: ["subject_name"] },
        { model: chapters, attributes: ["chapter_name"] },
      ],
      order: [["subject_id", "ASC"]],
    });

    res.status(200).json({
      statusCode: 200,
      statusText: "success",
      message: "Get report all performance students success.",
      result: resData,
    });
  } catch (error) {
    res.status(400).send(error.message);
  }
};

method.topThreeStudent = async (req, res) => {
  try {
    let n = 1,
      pushSourceData = [];
    while (n <= 12) {
      const resData = await reports.findAll({
        where: { grade: n },
        attributes: ["grade", "score"],
        include: [
          { model: students, attributes: ["name"] },
          { model: subjects, attributes: ["subject_name"] },
          { model: chapters, attributes: ["chapter_name"] },
        ],
        order: [["score", "DESC"]],
        limit: 3,
      });
      n++;
      pushSourceData.push(resData);
    }

    res.status(200).json({
      statusCode: 200,
      statusText: "success",
      message: "Get Top 3 students of each grade success.",
      result: { data: pushSourceData },
    });
  } catch (error) {
    res.status(400).send(error.message);
  }
};

method.listStudentsScoreBelowAverage = async (req, res) => {
  try {
    const resDataAvg = await reports.findAll({
      attributes: [
        "grade",
        "subject_id",
        "chapter_id",
        [
          Sequelize.fn("ROUND", Sequelize.fn("avg", Sequelize.col("score")), 1),
          "avg_score",
        ],
      ],
      group: ["reports.subject_id", "reports.grade", "reports.chapter_id"],
      order: [["chapter_id", "ASC"]],
      raw: true,
    });

    let pushData = [];
    for (let i = 0; i < resDataAvg.length; i++) {
      const resData = await reports.findAll({
        where: {
          [Op.and]: [
            { chapter_id: resDataAvg[i].chapter_id },
            { score: { [Op.lt]: parseFloat(resDataAvg[i].avg_score) } },
          ],
        },
        attributes: ["grade", "score"],
        include: [
          { model: students, attributes: ["name"] },
          { model: subjects, attributes: ["subject_name"] },
          { model: chapters, attributes: ["chapter_name"] },
        ],
      });
      pushData.push(resData);
    }

    res.status(200).json({
      statusCode: 200,
      statusText: "success",
      message:
        "Get a List of student name and their grade that have below average scores in ALL subjects success.",
      result: { data: pushData },
    });
  } catch (error) {
    res.status(400).send(error.message);
  }
};

method.reportStudentPerId = async (req, res) => {
  try {
    const resData = await reports.findAll({
      attributes: ["grade", "score"],
      where: { student_id: req.params.id },
      include: [
        { model: students, attributes: ["name"] },
        { model: subjects, attributes: ["subject_name"] },
        { model: chapters, attributes: ["chapter_name"] },
      ],
      order: [["report_id", "ASC"]],
    });
    res.status(200).json({
      statusCode: 200,
      statusText: "success",
      message: "Get Report perId success.",
      result: resData,
    });
  } catch (error) {
    res.status(400).send(error.message);
  }
};

module.exports = method;
