require("dotenv").config();

module.exports = {
  development: {
    username: process.env.username,
    password: process.env.password,
    database: process.env.database,
    host: process.env.host,
    dialect: "postgres",
    define: {
      timestamps: false,
    },
    dialectOptions: {
      ssl: { rejectUnauthorized: false },
    },
  },
  test: {
    username: process.env.username,
    password: process.env.password,
    database: process.env.database,
    host: process.env.host,
    dialect: "postgres",
    define: {
      timestamps: false,
    },
    dialectOptions: {
      ssl: { rejectUnauthorized: false },
    },
  },
  production: {
    username: process.env.username,
    password: process.env.password,
    database: process.env.database,
    host: process.env.host,
    dialect: "postgres",
    define: {
      timestamps: false,
    },
    dialectOptions: {
      ssl: { rejectUnauthorized: false },
    },
  },
};
