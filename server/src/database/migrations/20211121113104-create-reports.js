"use strict";
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable("reports", {
      report_id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      student_id: {
        type: Sequelize.INTEGER,
        references: { model: "students", key: "student_id" },
      },
      grade: {
        type: Sequelize.INTEGER,
      },
      subject_id: {
        type: Sequelize.INTEGER,
        references: { model: "subjects", key: "subject_id" },
      },
      chapter_id: {
        type: Sequelize.INTEGER,
        references: { model: "chapters", key: "chapter_id" },
      },
      score: {
        type: Sequelize.INTEGER,
      },
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable("reports");
  },
};
