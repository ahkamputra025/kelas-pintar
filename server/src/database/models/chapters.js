"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class chapters extends Model {
    static associate({ reports }) {
      // define association here
      this.hasMany(reports, { foreignKey: "chapter_id" });
    }
  }
  chapters.init(
    {
      chapter_id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
      },
      chapter_name: DataTypes.STRING,
      grade: DataTypes.INTEGER,
      subject_id: DataTypes.INTEGER,
    },
    {
      sequelize,
      modelName: "chapters",
    }
  );
  return chapters;
};
