"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class reports extends Model {
    static associate({ subjects, chapters, students }) {
      // define association here
      this.belongsTo(subjects, { foreignKey: "subject_id" });
      this.belongsTo(chapters, { foreignKey: "chapter_id" });
      this.belongsTo(students, { foreignKey: "student_id" });
    }
  }
  reports.init(
    {
      report_id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
      },
      student_id: DataTypes.INTEGER,
      grade: DataTypes.INTEGER,
      subject_id: DataTypes.INTEGER,
      chapter_id: DataTypes.INTEGER,
      score: DataTypes.INTEGER,
    },
    {
      sequelize,
      modelName: "reports",
    }
  );
  return reports;
};
