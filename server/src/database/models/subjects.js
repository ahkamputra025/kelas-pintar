"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class subjects extends Model {
    static associate({ reports }) {
      // define association here
      // this.belongsTo(reports, { foreignKey: "subject_id" });
      this.hasMany(reports, { foreignKey: "subject_id" });
    }
  }
  subjects.init(
    {
      subject_id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
      },
      subject_name: DataTypes.STRING,
      grade: DataTypes.INTEGER,
    },
    {
      sequelize,
      modelName: "subjects",
    }
  );
  return subjects;
};
