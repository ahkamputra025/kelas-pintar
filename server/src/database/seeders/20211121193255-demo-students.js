"use strict";
const faker = require("faker");
function generateStudent() {
  let student = [];
  for (let i = 1; i <= 61; i++) {
    let Name = faker.name.findName();
    student.push({
      name: Name,
    });
  }
  return { data: student };
}
let dataObj = generateStudent();

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert(
      "students",
      [
        {
          name: dataObj.data[0].name,
          grade: 1,
        },
        {
          name: dataObj.data[1].name,
          grade: 1,
        },
        {
          name: dataObj.data[2].name,
          grade: 1,
        },
        {
          name: dataObj.data[3].name,
          grade: 1,
        },
        {
          name: dataObj.data[4].name,
          grade: 1,
        },
        {
          name: dataObj.data[5].name,
          grade: 2,
        },
        {
          name: dataObj.data[6].name,
          grade: 2,
        },
        {
          name: dataObj.data[7].name,
          grade: 2,
        },
        {
          name: dataObj.data[8].name,
          grade: 2,
        },
        {
          name: dataObj.data[9].name,
          grade: 2,
        },
        {
          name: dataObj.data[10].name,
          grade: 3,
        },
        {
          name: dataObj.data[11].name,
          grade: 3,
        },
        {
          name: dataObj.data[12].name,
          grade: 3,
        },
        {
          name: dataObj.data[13].name,
          grade: 3,
        },
        {
          name: dataObj.data[14].name,
          grade: 3,
        },
        {
          name: dataObj.data[15].name,
          grade: 4,
        },
        {
          name: dataObj.data[16].name,
          grade: 4,
        },
        {
          name: dataObj.data[17].name,
          grade: 4,
        },
        {
          name: dataObj.data[18].name,
          grade: 4,
        },
        {
          name: dataObj.data[19].name,
          grade: 4,
        },
        {
          name: dataObj.data[20].name,
          grade: 5,
        },
        {
          name: dataObj.data[21].name,
          grade: 5,
        },
        {
          name: dataObj.data[22].name,
          grade: 5,
        },
        {
          name: dataObj.data[23].name,
          grade: 5,
        },
        {
          name: dataObj.data[24].name,
          grade: 5,
        },
        {
          name: dataObj.data[25].name,
          grade: 6,
        },
        {
          name: dataObj.data[26].name,
          grade: 6,
        },
        {
          name: dataObj.data[27].name,
          grade: 6,
        },
        {
          name: dataObj.data[28].name,
          grade: 6,
        },
        {
          name: dataObj.data[29].name,
          grade: 6,
        },
        {
          name: dataObj.data[30].name,
          grade: 7,
        },
        {
          name: dataObj.data[31].name,
          grade: 7,
        },
        {
          name: dataObj.data[32].name,
          grade: 7,
        },
        {
          name: dataObj.data[33].name,
          grade: 7,
        },
        {
          name: dataObj.data[34].name,
          grade: 7,
        },
        {
          name: dataObj.data[35].name,
          grade: 8,
        },
        {
          name: dataObj.data[36].name,
          grade: 8,
        },
        {
          name: dataObj.data[37].name,
          grade: 8,
        },
        {
          name: dataObj.data[38].name,
          grade: 8,
        },
        {
          name: dataObj.data[39].name,
          grade: 8,
        },
        {
          name: dataObj.data[40].name,
          grade: 9,
        },
        {
          name: dataObj.data[41].name,
          grade: 9,
        },
        {
          name: dataObj.data[42].name,
          grade: 9,
        },
        {
          name: dataObj.data[43].name,
          grade: 9,
        },
        {
          name: dataObj.data[44].name,
          grade: 9,
        },
        {
          name: dataObj.data[45].name,
          grade: 10,
        },
        {
          name: dataObj.data[46].name,
          grade: 10,
        },
        {
          name: dataObj.data[47].name,
          grade: 10,
        },
        {
          name: dataObj.data[48].name,
          grade: 10,
        },
        {
          name: dataObj.data[49].name,
          grade: 10,
        },
        {
          name: dataObj.data[50].name,
          grade: 11,
        },
        {
          name: dataObj.data[51].name,
          grade: 11,
        },
        {
          name: dataObj.data[52].name,
          grade: 11,
        },
        {
          name: dataObj.data[53].name,
          grade: 11,
        },
        {
          name: dataObj.data[54].name,
          grade: 11,
        },

        {
          name: dataObj.data[55].name,
          grade: 12,
        },
        {
          name: dataObj.data[56].name,
          grade: 12,
        },
        {
          name: dataObj.data[57].name,
          grade: 12,
        },
        {
          name: dataObj.data[58].name,
          grade: 12,
        },
        {
          name: dataObj.data[59].name,
          grade: 12,
        },
      ],
      {}
    );
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  },
};
