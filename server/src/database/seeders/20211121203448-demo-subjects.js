"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert(
      "subjects",
      [
        {
          subject_name: "Math",
          grade: 1,
        },
        {
          subject_name: "English",
          grade: 1,
        },
        {
          subject_name: "Indonesian",
          grade: 1,
        },
        {
          subject_name: "Science",
          grade: 1,
        },

        {
          subject_name: "Math",
          grade: 2,
        },
        {
          subject_name: "English",
          grade: 2,
        },
        {
          subject_name: "Indonesian",
          grade: 2,
        },
        {
          subject_name: "Science",
          grade: 2,
        },

        {
          subject_name: "Math",
          grade: 3,
        },
        {
          subject_name: "English",
          grade: 3,
        },
        {
          subject_name: "Indonesian",
          grade: 3,
        },
        {
          subject_name: "Science",
          grade: 3,
        },

        {
          subject_name: "Math",
          grade: 4,
        },
        {
          subject_name: "English",
          grade: 4,
        },
        {
          subject_name: "Indonesian",
          grade: 4,
        },
        {
          subject_name: "Science",
          grade: 4,
        },

        {
          subject_name: "Math",
          grade: 5,
        },
        {
          subject_name: "English",
          grade: 5,
        },
        {
          subject_name: "Indonesian",
          grade: 5,
        },
        {
          subject_name: "Science",
          grade: 5,
        },

        {
          subject_name: "Math",
          grade: 6,
        },
        {
          subject_name: "English",
          grade: 6,
        },
        {
          subject_name: "Indonesian",
          grade: 6,
        },
        {
          subject_name: "Science",
          grade: 6,
        },

        {
          subject_name: "Math",
          grade: 7,
        },
        {
          subject_name: "English",
          grade: 7,
        },
        {
          subject_name: "Indonesian",
          grade: 7,
        },
        {
          subject_name: "Science",
          grade: 7,
        },

        {
          subject_name: "Math",
          grade: 8,
        },
        {
          subject_name: "English",
          grade: 8,
        },
        {
          subject_name: "Indonesian",
          grade: 8,
        },
        {
          subject_name: "Science",
          grade: 8,
        },

        {
          subject_name: "Math",
          grade: 9,
        },
        {
          subject_name: "English",
          grade: 9,
        },
        {
          subject_name: "Indonesian",
          grade: 9,
        },
        {
          subject_name: "Science",
          grade: 9,
        },

        {
          subject_name: "Math",
          grade: 10,
        },
        {
          subject_name: "English",
          grade: 10,
        },
        {
          subject_name: "Indonesian",
          grade: 10,
        },
        {
          subject_name: "Science",
          grade: 10,
        },

        {
          subject_name: "Math",
          grade: 11,
        },
        {
          subject_name: "English",
          grade: 11,
        },
        {
          subject_name: "Indonesian",
          grade: 11,
        },
        {
          subject_name: "Science",
          grade: 11,
        },

        {
          subject_name: "Math",
          grade: 12,
        },
        {
          subject_name: "English",
          grade: 12,
        },
        {
          subject_name: "Indonesian",
          grade: 12,
        },
        {
          subject_name: "Science",
          grade: 12,
        },
      ],
      {}
    );
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  },
};
