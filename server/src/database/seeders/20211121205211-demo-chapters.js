"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert(
      "chapters",
      [
        {
          chapter_name: "Chapter I Whole number",
          grade: 1,
          subject_id: 1,
        },
        {
          chapter_name: "Chapter II Addition and Substraction",
          grade: 1,
          subject_id: 1,
        },
        {
          chapter_name: "Chapter III Division and Multiplication",
          grade: 1,
          subject_id: 1,
        },

        {
          chapter_name: "Chapter I Alphabet",
          grade: 1,
          subject_id: 2,
        },
        {
          chapter_name: "Chapter II Animals",
          grade: 1,
          subject_id: 2,
        },
        {
          chapter_name: "Chapter III Numbers",
          grade: 1,
          subject_id: 2,
        },

        {
          chapter_name: "Chapter I Aku Dan Keluargaku",
          grade: 1,
          subject_id: 3,
        },
        {
          chapter_name: "Chapter II Permainan",
          grade: 1,
          subject_id: 3,
        },
        {
          chapter_name: "Chapter III Pekerjaan",
          grade: 1,
          subject_id: 3,
        },

        {
          chapter_name: "Chapter I My Body",
          grade: 1,
          subject_id: 4,
        },
        {
          chapter_name: "Chapter II I'm growing healthy",
          grade: 1,
          subject_id: 4,
        },
        {
          chapter_name: "Chapter III My environment is healthy",
          grade: 1,
          subject_id: 4,
        },

        {
          chapter_name: "Chapter I Number",
          grade: 2,
          subject_id: 5,
        },
        {
          chapter_name: "Chapter II Time Measurement",
          grade: 2,
          subject_id: 5,
        },
        {
          chapter_name: "Chapter III Length Measurement",
          grade: 2,
          subject_id: 5,
        },

        {
          chapter_name: "Chapter I My Family",
          grade: 2,
          subject_id: 6,
        },
        {
          chapter_name: "Chapter II Telling Time",
          grade: 2,
          subject_id: 6,
        },
        {
          chapter_name: "Chapter III Foods and Drink",
          grade: 2,
          subject_id: 6,
        },

        {
          chapter_name: "Chapter I Kegiatan",
          grade: 2,
          subject_id: 7,
        },
        {
          chapter_name: "Chapter II Kegiatan Keluarga",
          grade: 2,
          subject_id: 7,
        },
        {
          chapter_name: "Chapter III Hiburan",
          grade: 2,
          subject_id: 7,
        },

        {
          chapter_name: "Chapter I Animal and Plant Body Parts",
          grade: 2,
          subject_id: 8,
        },
        {
          chapter_name: "Chapter II Growth in Animals and Plants",
          grade: 2,
          subject_id: 8,
        },
        {
          chapter_name: "Chapter III Where Living Creatures Live",
          grade: 2,
          subject_id: 8,
        },

        {
          chapter_name:
            "Chapter I Determining the Location of Numbers on the Number Line",
          grade: 3,
          subject_id: 9,
        },
        {
          chapter_name: "Chapter II Operation Count Addition and Subtraction",
          grade: 3,
          subject_id: 9,
        },
        {
          chapter_name: "Chapter III Multiplication and Division Hitung",
          grade: 3,
          subject_id: 9,
        },

        {
          chapter_name:
            "Chapter I Respond by repeating new vocabulary out loud",
          grade: 3,
          subject_id: 10,
        },
        {
          chapter_name:
            "Chapter II Respond by taking actions according to instructions in an acceptable manner",
          grade: 3,
          subject_id: 10,
        },
        {
          chapter_name:
            "Chapter III Conversing to ask/give information in an acceptable manner involving speech acts: introducing oneself, asking for ownership, asking for activities that someone is currently doing",
          grade: 3,
          subject_id: 10,
        },

        {
          chapter_name: "Chapter I Makhluk Hidup",
          grade: 3,
          subject_id: 11,
        },
        {
          chapter_name: "Chapter II Lingkungan",
          grade: 3,
          subject_id: 11,
        },
        {
          chapter_name: "Chapter III Kerja Sama",
          grade: 3,
          subject_id: 11,
        },

        {
          chapter_name:
            "Chapter I Characteristics and Needs of Living Creatures",
          grade: 3,
          subject_id: 12,
        },
        {
          chapter_name: "Chapter II Changes in Living Things",
          grade: 3,
          subject_id: 12,
        },
        {
          chapter_name: "Chapter III Healthy and Unhealthy Environment",
          grade: 3,
          subject_id: 12,
        },

        {
          chapter_name: "Chapter I COUNT OPERATIONS",
          grade: 4,
          subject_id: 13,
        },
        {
          chapter_name: "Chapter II MULTIPLES AND FACTORS OF A NUMBER",
          grade: 4,
          subject_id: 13,
        },
        {
          chapter_name:
            "Chapter III MEASUREMENT OF ANGLE, TIME, LENGTH, AND WEIGHT",
          grade: 4,
          subject_id: 13,
        },

        {
          chapter_name: "Chapter I Introduction",
          grade: 4,
          subject_id: 14,
        },
        {
          chapter_name: "Chapter II Flowers and Trees",
          grade: 4,
          subject_id: 14,
        },
        {
          chapter_name: "Chapter III Zoo",
          grade: 4,
          subject_id: 14,
        },

        {
          chapter_name: "Chapter I Tempat Umum",
          grade: 4,
          subject_id: 15,
        },
        {
          chapter_name: "Chapter II Pengalaman",
          grade: 4,
          subject_id: 15,
        },
        {
          chapter_name: "Chapter III Kegiatan",
          grade: 4,
          subject_id: 15,
        },

        {
          chapter_name:
            "Chapter I The Human Skeleton, its functions and maintenance",
          grade: 4,
          subject_id: 16,
        },
        {
          chapter_name:
            "Chapter II Human Sense Tool, function, and maintenance",
          grade: 4,
          subject_id: 16,
        },
        {
          chapter_name: "Chapter III Parts of plants and their functions",
          grade: 4,
          subject_id: 16,
        },

        {
          chapter_name: "Chapter I Fraction Count Operation",
          grade: 5,
          subject_id: 17,
        },
        {
          chapter_name: "Chapter II Speed and Discharge",
          grade: 5,
          subject_id: 17,
        },
        {
          chapter_name: "Chapter III Scale",
          grade: 5,
          subject_id: 17,
        },

        {
          chapter_name: "Chapter I Things in the Classroom",
          grade: 5,
          subject_id: 18,
        },
        {
          chapter_name: "Chapter II Things Around School",
          grade: 5,
          subject_id: 18,
        },
        {
          chapter_name: "Chapter III Greetings",
          grade: 5,
          subject_id: 18,
        },

        {
          chapter_name: "Chapter I Ayo Bertani",
          grade: 5,
          subject_id: 19,
        },
        {
          chapter_name: "Chapter II Persahabatan",
          grade: 5,
          subject_id: 19,
        },
        {
          chapter_name: "Chapter III Lingkungan",
          grade: 5,
          subject_id: 19,
        },

        {
          chapter_name: "Chapter I Functions of Human and Animal Organs",
          grade: 5,
          subject_id: 20,
        },
        {
          chapter_name: "Chapter II Green plants",
          grade: 5,
          subject_id: 20,
        },
        {
          chapter_name: "Chapter III Objects and their properties",
          grade: 5,
          subject_id: 20,
        },

        {
          chapter_name: "Chapter I Integer Count Operation",
          grade: 6,
          subject_id: 21,
        },
        {
          chapter_name: "Chapter II Timed Volume Measurement",
          grade: 6,
          subject_id: 21,
        },
        {
          chapter_name: "Chapter III Calculating Area",
          grade: 6,
          subject_id: 21,
        },

        {
          chapter_name: "Chapter I Direction and Location",
          grade: 6,
          subject_id: 22,
        },
        {
          chapter_name: "Chapter II Holiday",
          grade: 6,
          subject_id: 22,
        },
        {
          chapter_name: "Chapter III Describing People and Object",
          grade: 6,
          subject_id: 22,
        },

        {
          chapter_name: "Chapter I Pertanian",
          grade: 6,
          subject_id: 23,
        },
        {
          chapter_name: "Chapter II Hiburan",
          grade: 6,
          subject_id: 23,
        },
        {
          chapter_name: "Chapter III Lingkungan Bersih dan Sehat",
          grade: 6,
          subject_id: 23,
        },

        {
          chapter_name: "Chapter I Special Characteristics of Living Creatures",
          grade: 6,
          subject_id: 24,
        },
        {
          chapter_name:
            "Chapter II Special Characteristics of Living Creatures",
          grade: 6,
          subject_id: 24,
        },
        {
          chapter_name: "Chapter III Ecosystem Balance",
          grade: 6,
          subject_id: 24,
        },

        {
          chapter_name: "Chapter I Number Pattern",
          grade: 7,
          subject_id: 25,
        },
        {
          chapter_name: "Chapter II Cartesian Field",
          grade: 7,
          subject_id: 25,
        },
        {
          chapter_name: "Chapter III Relations and Functions",
          grade: 7,
          subject_id: 25,
        },

        {
          chapter_name: "Chapter I Greeting",
          grade: 7,
          subject_id: 26,
        },
        {
          chapter_name: "Chapter II Partings",
          grade: 7,
          subject_id: 26,
        },
        {
          chapter_name: "Chapter III Thanking and Apologizing",
          grade: 7,
          subject_id: 26,
        },

        {
          chapter_name: "Chapter I Teks Deskripsi",
          grade: 7,
          subject_id: 27,
        },
        {
          chapter_name: "Chapter II Teks Cerita Fantasi",
          grade: 7,
          subject_id: 27,
        },
        {
          chapter_name: "Chapter III Teks Prosedur",
          grade: 7,
          subject_id: 27,
        },

        {
          chapter_name: "Chapter I Classification of Living Things",
          grade: 7,
          subject_id: 28,
        },
        {
          chapter_name: "Chapter II Temperature and Changes",
          grade: 7,
          subject_id: 28,
        },
        {
          chapter_name: "Chapter III Heat and its displacement",
          grade: 7,
          subject_id: 28,
        },

        {
          chapter_name: "Chapter I Number Pattern",
          grade: 8,
          subject_id: 29,
        },
        {
          chapter_name: "Chapter II Coordinate System",
          grade: 8,
          subject_id: 29,
        },
        {
          chapter_name: "Chapter III Relaxation and Function",
          grade: 8,
          subject_id: 29,
        },

        {
          chapter_name: "Chapter I It’s English Time!",
          grade: 8,
          subject_id: 30,
        },
        {
          chapter_name: "Chapter II We can do it, and we will do it",
          grade: 8,
          subject_id: 30,
        },
        {
          chapter_name: "Chapter III We know what to do",
          grade: 8,
          subject_id: 30,
        },

        {
          chapter_name: "Chapter I Berita Seputar Indonesia",
          grade: 8,
          subject_id: 31,
        },
        {
          chapter_name: "Chapter II Iklan Saran Komunikasi",
          grade: 8,
          subject_id: 31,
        },
        {
          chapter_name: "Chapter III Teks eksposisi dalam media massa",
          grade: 8,
          subject_id: 31,
        },

        {
          chapter_name: "Chapter I Movement of Living Things and Creatures",
          grade: 8,
          subject_id: 32,
        },
        {
          chapter_name: "Chapter II Efforts and Simple Planes in Daily Life",
          grade: 8,
          subject_id: 32,
        },
        {
          chapter_name: "Chapter III Plant Structure and Function",
          grade: 8,
          subject_id: 32,
        },

        {
          chapter_name: "Chapter I Powers and Root Forms",
          grade: 9,
          subject_id: 33,
        },
        {
          chapter_name: "Chapter II Quadratic Equations and Functions",
          grade: 9,
          subject_id: 33,
        },
        {
          chapter_name: "Chapter III Transformation",
          grade: 9,
          subject_id: 33,
        },

        {
          chapter_name: "Chapter I Congratulation	Download",
          grade: 9,
          subject_id: 34,
        },
        {
          chapter_name: "Chapter II Let’s live a healthy life!	Download",
          grade: 9,
          subject_id: 34,
        },
        {
          chapter_name: "Chapter III Be healthy, be happy!",
          grade: 9,
          subject_id: 34,
        },

        {
          chapter_name: "Chapter I Melaporkan hasil percobaan",
          grade: 9,
          subject_id: 35,
        },
        {
          chapter_name: "Chapter II Menyampaikan Pidato persuasif",
          grade: 9,
          subject_id: 35,
        },
        {
          chapter_name: "Chapter III Menyusun cerita pendek",
          grade: 9,
          subject_id: 35,
        },

        {
          chapter_name: "Chapter I Reproductive System in Humans",
          grade: 9,
          subject_id: 36,
        },
        {
          chapter_name: "Chapter II Reproductive System in Plants",
          grade: 9,
          subject_id: 36,
        },
        {
          chapter_name: "Chapter III Inheritance of Traits in Living Beings",
          grade: 9,
          subject_id: 36,
        },

        {
          chapter_name:
            "Chapter I Equations and Inequality Linear Absolute Values of One Variable",
          grade: 10,
          subject_id: 37,
        },
        {
          chapter_name: "Chapter II Three Variable System of Linear Equations",
          grade: 10,
          subject_id: 37,
        },
        {
          chapter_name: "Chapter III Function",
          grade: 10,
          subject_id: 37,
        },

        {
          chapter_name: "Chapter I Talking About Self",
          grade: 10,
          subject_id: 38,
        },
        {
          chapter_name: "Chapter II Congratulating and Complementing Others",
          grade: 10,
          subject_id: 38,
        },
        {
          chapter_name: "Chapter III What are You Going to Do Today",
          grade: 10,
          subject_id: 38,
        },

        {
          chapter_name: "Chapter I Menyusun Laporan Hasil Observasi",
          grade: 10,
          subject_id: 39,
        },
        {
          chapter_name: "Chapter II Mengembangkan Pendapat Dalam Eksposisi",
          grade: 10,
          subject_id: 39,
        },
        {
          chapter_name: "Chapter III Menyampaikan Ide Melalui Anekdot",
          grade: 10,
          subject_id: 39,
        },

        {
          chapter_name: "Chapter I Scientific Work",
          grade: 10,
          subject_id: 40,
        },
        {
          chapter_name: "Chapter II Classification of Living Things",
          grade: 10,
          subject_id: 40,
        },
        {
          chapter_name: "Chapter III Virus",
          grade: 10,
          subject_id: 40,
        },

        {
          chapter_name: "Chapter I Mathematical Induction",
          grade: 11,
          subject_id: 41,
        },
        {
          chapter_name: "Chapter II Linear Program",
          grade: 11,
          subject_id: 41,
        },
        {
          chapter_name: "Chapter III Matrix",
          grade: 11,
          subject_id: 41,
        },

        {
          chapter_name: "Chapter I Offers & Suggestions",
          grade: 11,
          subject_id: 42,
        },
        {
          chapter_name: "Chapter II Opinion & Thoughts",
          grade: 11,
          subject_id: 42,
        },
        {
          chapter_name: "Chapter III Party Time",
          grade: 11,
          subject_id: 42,
        },

        {
          chapter_name: "Chapter I Menyusun Prosedur",
          grade: 11,
          subject_id: 43,
        },
        {
          chapter_name: "Chapter II Mempelajari Teks Eksplanasi",
          grade: 11,
          subject_id: 43,
        },
        {
          chapter_name: "Chapter III Mengelola Informasi dalam Ceramah",
          grade: 11,
          subject_id: 43,
        },

        {
          chapter_name: "Chapter I Equilibrium and Rotational Dynamics",
          grade: 11,
          subject_id: 44,
        },
        {
          chapter_name: "Chapter II Elasticity and Hooke's Law",
          grade: 11,
          subject_id: 44,
        },
        {
          chapter_name: "Chapter III Static Fluid",
          grade: 11,
          subject_id: 44,
        },

        {
          chapter_name: "Chapter I Flat Plane Geometry",
          grade: 12,
          subject_id: 45,
        },
        {
          chapter_name: "Chapter II Space Plane Geometry",
          grade: 12,
          subject_id: 45,
        },
        {
          chapter_name: "Chapter III Introduction to Statistics",
          grade: 12,
          subject_id: 45,
        },

        {
          chapter_name: "Chapter I May I Help You?",
          grade: 12,
          subject_id: 46,
        },
        {
          chapter_name: "Chapter II Why Don’t You Visit Seattle",
          grade: 12,
          subject_id: 46,
        },
        {
          chapter_name: "Chapter III Creating Captions",
          grade: 12,
          subject_id: 46,
        },

        {
          chapter_name: "Chapter I Membuat Surat Lamaran Pekerjaan",
          grade: 12,
          subject_id: 47,
        },
        {
          chapter_name: "Chapter II Menikmati Cerita Sejarah",
          grade: 12,
          subject_id: 47,
        },
        {
          chapter_name: "Chapter III Memahami Isu Terkini Lewat Editorial",
          grade: 12,
          subject_id: 47,
        },

        {
          chapter_name: "Chapter I Plant Growth and Development",
          grade: 12,
          subject_id: 48,
        },
        {
          chapter_name: "Chapter II Metabolism",
          grade: 12,
          subject_id: 48,
        },
        {
          chapter_name: "Chapter III Heredity",
          grade: 12,
          subject_id: 48,
        },
      ],
      {}
    );
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  },
};
