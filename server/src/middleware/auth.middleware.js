const jwt = require("jsonwebtoken");
const method = {};

method.authenticationTeacher = (req, res, next) => {
  const token = req.header("Authorization");
  if (!token)
    return res.status(401).json({ message: "Failed to authenticate token." });

  try {
    const verified = jwt.verify(token, process.env.SECRET_KEY);
    req.token = verified;
    const role = req.token.valDataJwt.role;
    if (role === "teacher") {
      next();
    } else {
      res.status(401).json({ message: "You are not teacher." });
    }
  } catch (error) {
    res.status(400).json({ message: "Token not verified." });
  }
};

method.authenticationStudent = (req, res, next) => {
  const token = req.header("Authorization");
  if (!token)
    return res.status(401).json({ message: "Failed to authenticate token." });

  try {
    const verified = jwt.verify(token, process.env.SECRET_KEY);
    req.token = verified;
    const role = req.token.valDataJwt.role;
    if (role === "student") {
      next();
    } else {
      res.status(401).json({ message: "You are not student." });
    }
  } catch (error) {
    res.status(400).json({ message: "Token not verified." });
  }
};

module.exports = method;
