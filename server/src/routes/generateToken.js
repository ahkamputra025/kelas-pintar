const express = require("express");
const router = express.Router();

const { generateToken } = require("../controllers/generetaToken");

router.post("/generateToken", generateToken);

module.exports = router;
