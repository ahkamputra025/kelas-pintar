const express = require("express");
const router = express.Router();

const { myReport } = require("../controllers/student.controller");
const { authenticationStudent } = require("../middleware/auth.middleware");

router.get("/myReport", authenticationStudent, myReport);

module.exports = router;
