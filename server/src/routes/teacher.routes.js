const express = require("express");
const router = express.Router();

const {
  reportAllStudent,
  topThreeStudent,
  listStudentsScoreBelowAverage,
  reportStudentPerId,
} = require("../controllers/teacher.controller");

const { authenticationTeacher } = require("../middleware/auth.middleware");

router.get("/reportAllStudent", authenticationTeacher, reportAllStudent);
router.get("/topThreeStudent", authenticationTeacher, topThreeStudent);
router.get(
  "/listStudentsScoreBelowAverage",
  authenticationTeacher,
  listStudentsScoreBelowAverage
);
router.get(
  "/reportStudentPerId/:id",
  authenticationTeacher,
  reportStudentPerId
);

module.exports = router;
